# connect-cas2-client

#### 项目介绍
nodejs实现cas客户端

#### 软件架构
软件架构说明
使用第三方包[connect-cas2](https://github.com/TencentWSRD/connect-cas2)


#### 安装教程

在项目当前目录：
1. npm init -y
   init初始化项目，生成package.json, -y表示yes
2. npm install connect-cas2 --save-dev
   npm install express body-parser express-session cookie-parser session-memory-store --save-dev

#### 使用说明

1. 直接运行node client.js
2. 访问localhost:3000
3. 注意：请确认你的cas-server可用，并且已经启动

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
